# CCNU 设计模式 大作业

## 1.1 简介

> 设计一个画笔程序，要求 ：
>
> - 支持画圆（椭圆）、矩形、直线……，支持随手写功能；
>
> - 可以改变画笔的颜色与粗细；
>
> - 可以保存为文件（专用格式），并且可以读取显示；
>
> - 可以另存为图像（支持JPG，BMP等常用的文件格式）；
>
> - 可以改变画笔的笔形，比如毛笔，蜡笔，铅笔等；
>
> - 可以实现redo, undo功能
>
> - 支持改变画布的背景，比如支持将一幅图像载入作为画布背景，此时就好像在图像上绘画
> - 支持更改主题
>
> 基于以上功能，使用不少于10个设计模式。

## 1.2 方案与设计模式

### 技术选型

为了实现GUI程序，在Java中有很多选择，主要有如下方案：

1、使用AWT。

> JDK自带，AWT 是重量级组件，由于依赖操作系统绘制，不好用不用。

**2、使用Swing。**

> JDK自带，Swing是轻量级包，重写了部分AWT组件，由于单独抽象剥离了操作系统的限制，所以多了不少组件。注意：Canvas组件是AWT组件，如果使用Canva作为画布将导致遮盖等BUG，所以要用Swing就全部用Swing的组件。
>
> 为了让UI不要太丑，所以引入了Swing的主题包`https://github.com/JFormDesigner/FlatLaf`

3、使用JavaFX。

> JDK11前在JDK中自带，可以整合Springboot，基于注解开发。写这样一个白板一两天就搞定。但是由于高度抽象会导致自己实现的设计模式无从下手。所以放弃。JavaFX有较好的脚手架项目`https://gitee.com/lwdillon/fx-falsework`

### 设计模式

```
单例模式
1、PaintConfig
2、MyCanvas
3、DrawListener
等等，只要全局唯一都可以用单例，此处只列举功能重要的


原型模式
CloneUtils 深拷贝对象的工具类

建造者模式
MainFrameDirector 主页面数据、事件初始化

抽象工厂模式
ThemeFactory 默认主题更改

策略模式
ThemeStrategy 自定义主题接入

适配器模式
ListenerAdapter 画图的监听器，使用接口适配器


模板方法模式
AbstractShape 各种形状的绘制


桥接模式
PaintData 给形状加上颜色属性，shape通过继承PaintData获得Color的能力。从而给形状赋上颜色

外观模式
CanvasUtils 将MyCanvas的细粒度接口进一步封装，提供更解耦的调用方式。


迭代器模式
IPair    蜡笔的一堆点，需要用迭代器模式遍历，用ArrayList也行，但主要是为了醋包饺子。

命令模式
redo undo 功能

工厂方法模式
TransFactory 导入导出按钮设计

备忘录模式
Memento 系统恢复机制

观察者模式
swing自带有，不再重复实现，参考下文
https://blog.csdn.net/weixin_39603573/article/details/114350041


装饰器模式
多种格式的导入导出存储等问题

代理模式
CaretakerProxy 打日志jdk动态代理


未涉及的设计模式：
1、解释器模式：解释器模式适用于表达式解析等场景，此项目无此类需求
2、组合模式：组合模式是应树形结构而生，此项目无树结构的操作需求。
3、享元模式：无需管理细粒度对象
4、责任链模式：没有责任链的处理需求
5、中介者模式：事件驱动可以理解为消息订阅，最佳实践是观察者模式
6、状态模式：UI级别的状态swing做的很好，业务上的状态在枚举中搞定，否则类太多
7、访问者模式：绘制处理可以用访问者重构，但那部分没处理好耦合性太强了，不想重构了。
```

## 1.3 如何编译运行

> 前提

```shell
mmciel@mmciel-MacBook ~ % java -version
java version "1.8.0_321"
Java(TM) SE Runtime Environment (build 1.8.0_321-b07)
Java HotSpot(TM) 64-Bit Server VM (build 25.321-b07, mixed mode)


mmciel@mmciel-MacBook ~ % mvn -v      
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: /Users/mmciel/mmciel-env/maven/apache-maven-3.6.3
Java version: 1.8.0_321, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk1.8.0_321.jdk/Contents/Home/jre
Default locale: zh_CN, platform encoding: UTF-8
OS name: "mac os x", version: "13.1", arch: "x86_64", family: "mac"
```

> 编译运行

```
参考普通Maven项目运行方法
```

## 1.4 如何运行demo

```
cd demo
java -jar Application.jar
```



## 1.5 效果图

![image-20221226153528674](img/image-20221226153528674.png)

![image-20221226153549130](img/image-20221226153549130.png)

![image-20221226153759744](img/image-20221226153759744.png)



## 1.6 其他

### 关于优化方向

- 项目中使用的大量单例可以使用Spring IOC容器托管，如果单单实现一个IOC容器，从成本上来说是值得的。可以参考other文件夹下的代码。
- 项目依赖JFromBuilder 8构建UI画面，这让建造者变得难以理解，所以可以剥离JFromBuilder，手动实现组件初始化。
- 项目在恢复数据的处理不够优雅，应该向外暴露粗粒度的绘制数据。同样的，将画布和数据耦合到一个类里的设计也不够好，需要改进。
- 全局配置类可以被随意修改，不够优雅。



### 关于更新

- 此项目不再更新

