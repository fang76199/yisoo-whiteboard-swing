package com.yisoo.homework.whiteboard.utils;

import java.awt.*;


/**
 * ColorUtils
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/19 13:20
 * @update none
 */
public class ColorUtils {

    /**
     * 获取对比度明显的颜色
     * @param originalColor
     * @return
     */
    public static Color contrastColor(Color originalColor){
        return new Color(255-originalColor.getRed(),255-originalColor.getGreen(),255-originalColor.getBlue());
    }
}
