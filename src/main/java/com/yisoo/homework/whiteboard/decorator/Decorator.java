package com.yisoo.homework.whiteboard.decorator;

import lombok.AllArgsConstructor;

/**
 * 关于设计模式：装饰者模式
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/24 12:58
 * @describe Decorator
 */
@AllArgsConstructor
public abstract class Decorator implements Output, Import {

    private Output output;
    private Import input;

    public Decorator(Output output) {
        this.output = output;
    }

    public Decorator(Import input) {
        this.input = input;
    }

    @Override
    public void output(String path) {
        output.output(path);
    }

    @Override
    public void input(String path) {
        input.input(path);
    }
}
