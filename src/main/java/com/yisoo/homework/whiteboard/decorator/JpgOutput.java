package com.yisoo.homework.whiteboard.decorator;

import com.yisoo.homework.whiteboard.ui.canvas.MyCanvas;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/24 12:55
 * @describe PngOutput
 */
public class JpgOutput implements Output{


    @Override
    public void output(String path) {
        BufferedImage image = MyCanvas.getImage();
        try {
            ImageIO.write(image, "jpg", new File(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
