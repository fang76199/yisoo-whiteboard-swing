package com.yisoo.homework.whiteboard.decorator;

import com.yisoo.homework.whiteboard.ui.canvas.MyCanvas;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/24 12:55
 * @describe PngOutput
 */
public class FileOutput implements Output{


    @Override
    public void output(String path) {
        MyCanvas canvas = MyCanvas.getInstance();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(Files.newOutputStream(Paths.get(path)));
            oos.writeObject(canvas.createMemento());
            oos.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
