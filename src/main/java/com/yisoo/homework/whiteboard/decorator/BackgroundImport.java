package com.yisoo.homework.whiteboard.decorator;

import com.yisoo.homework.whiteboard.utils.CanvasUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/24 12:55
 * @describe PngOutput
 */
public class BackgroundImport implements Import{

    @Override
    public void input(String path) {
        try {
            BufferedImage image = ImageIO.read(Files.newInputStream(Paths.get(path)));
            CanvasUtils.addImage(image).redraw().show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
