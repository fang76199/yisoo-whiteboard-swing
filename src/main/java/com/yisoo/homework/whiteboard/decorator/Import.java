package com.yisoo.homework.whiteboard.decorator;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/24 13:29
 * @describe Import
 */
public interface Import {
    void input(String path);
}
