package com.yisoo.homework.whiteboard.decorator;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/24 12:54
 * @describe Output
 */
public interface Output {

    void output(String path);
}
