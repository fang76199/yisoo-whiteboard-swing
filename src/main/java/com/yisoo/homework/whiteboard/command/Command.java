package com.yisoo.homework.whiteboard.command;

import com.yisoo.homework.whiteboard.ui.component.shape.AbstractShape;

/**
 * TODO
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/22 14:55
 * @update none
 */
public interface Command {
    void execute(AbstractShape shape, Boolean isShow);

    void undo(Boolean isShow);

    void redo(Boolean isShow);
}
