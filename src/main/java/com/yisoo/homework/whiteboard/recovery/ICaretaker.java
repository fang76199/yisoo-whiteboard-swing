package com.yisoo.homework.whiteboard.recovery;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 16:43
 * @describe Caretaker
 */

public interface ICaretaker  {

    void loadBakFile();

    void saveBakFile();

    void recovery();

    void setMemento(Memento memento);
}
