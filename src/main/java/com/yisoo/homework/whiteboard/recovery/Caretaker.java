package com.yisoo.homework.whiteboard.recovery;

import com.yisoo.homework.whiteboard.ui.canvas.MyCanvas;
import com.yisoo.homework.whiteboard.utils.CanvasUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 16:43
 * @describe Caretaker
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class Caretaker implements ICaretaker {
    private Memento memento;

    private final static String BAK_FILE_PATH = "./yisoo.bak";


    public void loadBakFile(){
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(BAK_FILE_PATH);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object o = ois.readObject();
            ois.close();
            memento = (Memento)o;
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void saveBakFile(){
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(Files.newOutputStream(Paths.get(BAK_FILE_PATH)));
            oos.writeObject(memento);
            oos.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void recovery(){
        loadBakFile();
        MyCanvas canvas = MyCanvas.getInstance();
        canvas.restoreMemento(this.memento);
        CanvasUtils.reset().addImage().redraw().show();
    }
}
