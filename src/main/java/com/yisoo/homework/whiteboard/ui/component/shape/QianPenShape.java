package com.yisoo.homework.whiteboard.ui.component.shape;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.awt.*;

/**
 * TODO
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/21 0:03
 * @update none
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class QianPenShape extends PenShape {

    public QianPenShape() {
        super();
    }

    public QianPenShape(int x1, int y1, int x2, int y2) {
        super(x1, y1, x2, y2);
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.drawLine(this.getX1(), this.getY1(), this.getX2(), this.getY2());
    }
}
