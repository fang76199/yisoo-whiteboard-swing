package com.yisoo.homework.whiteboard.ui.listener;

import com.yisoo.homework.whiteboard.constant.DrawConst;
import com.yisoo.homework.whiteboard.recovery.Caretaker;
import com.yisoo.homework.whiteboard.recovery.CaretakerProxy;
import com.yisoo.homework.whiteboard.recovery.ICaretaker;
import com.yisoo.homework.whiteboard.ui.canvas.MyCanvas;
import com.yisoo.homework.whiteboard.ui.config.PaintConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * ListenerAdapter
 * 监听器适配器，精简接口
 *
 * 关于设计模式：适配器模式
 * 适配器模式主要分为：类适配器、对象适配器、接口适配器，此处使用接口适配器。
 *
 * 接口适配器的典型应用就是为接口瘦身，此处很合适。但是swing提供了对应的适配器类，可以直接使用。此处为了满足课设要求。
 *
 * 类适配器：继承类+实现接口
 * 对象适配器：要适配的接口作为属性耦合到适配器中
 * 接口适配器：继承一堆接口，提供空实现
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/20 16:40
 * @update none
 */
public abstract class ListenerAdapter implements MouseListener, MouseMotionListener, ActionListener {

    /**
     * 绑定画笔配置
     */
    protected final PaintConfig paintConfig = PaintConfig.getInstance();
    /**
     * 绑定画板
     */
    protected final MyCanvas canvas = MyCanvas.getInstance();

    /**
     * ActionListener
     * 发生操作时调用
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

    }

    /**
     * MouseListener
     * 鼠标点击，释放后调用
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    /**
     * MouseListener
     * 鼠标按压
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {
        ICaretaker caretaker = (ICaretaker)new CaretakerProxy(new Caretaker()).getProxy();
        caretaker.setMemento(canvas.createMemento());
        caretaker.saveBakFile();
    }

    /**
     * MouseListener
     * 鼠标释放
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * MouseListener
     * 鼠标进入组件
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        refreshCursor(e);
    }

    /**
     * MouseListener
     * 鼠标离开组件
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {
        putCursor(e, Cursor.DEFAULT_CURSOR);
    }


    /**
     * MouseMotionListener
     * 鼠标拖动事件
     * @param e
     */
    @Override
    public void mouseDragged(MouseEvent e) {

    }

    /**
     * MouseMotionListener
     * 鼠标光标移动到组件但未按下任何按钮时,动一下触发一下
     * @param e
     */
    @Override
    public void mouseMoved(MouseEvent e) {

    }


    private void refreshCursor(MouseEvent e) {
        // 插入文字
        if (DrawConst.BrushStyleEnum.WORD.equals(this.paintConfig.getStyle())) {
            putCursor(e, Cursor.TEXT_CURSOR);
        }
        // 橡皮
        else if (DrawConst.BrushStyleEnum.ERASE.equals(this.paintConfig.getStyle())) {
            putCursor(e, Cursor.HAND_CURSOR);
        }
        // 其他形状
        else {
            putCursor(e, Cursor.CROSSHAIR_CURSOR);
        }
    }

    private void putCursor(MouseEvent e, Integer i) {
        JFrame topFrame = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, e.getComponent());
        topFrame.setCursor(new Cursor(i));
    }
}
