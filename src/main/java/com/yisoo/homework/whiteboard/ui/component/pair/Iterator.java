package com.yisoo.homework.whiteboard.ui.component.pair;

/**
 * 关于设计模式：迭代器模式
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/22 13:42
 * @update none
 */
public interface Iterator<T>  {
    /**
     * 移动到聚合对象中下一个元素
     */
    T next();
    /**
     * 判断是否已经移动到聚合对象的最后一个元素
     */
    boolean hasNext();

    /**
     * 获取当前下标
     */
    int getIndex();
}
