package com.yisoo.homework.whiteboard.ui.theme.frame;

import com.formdev.flatlaf.FlatDarkLaf;

public class DarkFrameTheme extends FrameTheme{
    public DarkFrameTheme() {
        this.setFlatLaf(new FlatDarkLaf());
    }
}
