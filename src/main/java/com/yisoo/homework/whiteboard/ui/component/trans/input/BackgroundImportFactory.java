package com.yisoo.homework.whiteboard.ui.component.trans.input;

import com.yisoo.homework.whiteboard.decorator.BackgroundDecorator;
import com.yisoo.homework.whiteboard.decorator.BackgroundImport;
import com.yisoo.homework.whiteboard.ui.component.trans.TransFactory;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 15:38
 * @describe BackgroundImportFactory
 */
public class BackgroundImportFactory implements TransFactory {
    @Override
    public JMenuItem createItem() {
        BackgroundMenuItem item = new BackgroundMenuItem();

        item.addActionListener(e -> new BackgroundDecorator(new BackgroundImport()).input());

        return item;
    }
}