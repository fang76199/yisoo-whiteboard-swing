package com.yisoo.homework.whiteboard.ui.component.trans.output;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 11:31
 * @describe JPGMenuItem
 */
public class JPGMenuItem extends JMenuItem {

    public JPGMenuItem(){
        super("导出为JPG");
    }
}
