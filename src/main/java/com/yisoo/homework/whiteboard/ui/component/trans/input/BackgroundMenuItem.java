package com.yisoo.homework.whiteboard.ui.component.trans.input;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 15:36
 * @describe BackgroundMenuItem
 */
public class BackgroundMenuItem extends JMenuItem {
    public BackgroundMenuItem(){
        super("导入背景图片");
    }
}
