package com.yisoo.homework.whiteboard.ui.canvas;


import com.yisoo.homework.whiteboard.recovery.Memento;
import com.yisoo.homework.whiteboard.ui.component.shape.AbstractShape;
import com.yisoo.homework.whiteboard.ui.config.PaintConfig;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;


/**
 * MyCanvas
 * 关于设计模式：单例模式
 * 懒汉式 静态内部类方式
 * <p>
 * 本类很复杂，涉及多种设计模式，充当其他模式的调用者或者实现者等角色
 * 例如：备忘录模式，享元模式等。
 * <p>
 * 继承JPanel而不是Canvas的原因：https://blog.csdn.net/weixin_43797829/article/details/106530283
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/18 23:26
 * @update none
 */
@Data
public final class MyCanvas extends JPanel implements IMyCanvas {

    private final static long serialVersionUID = 1L;
    /**
     * 存储图形
     */
    private List<AbstractShape> shapes = new ArrayList<AbstractShape>();
    /**
     * 绑定画笔配置
     */
    @Getter
    private static final PaintConfig paintConfig = PaintConfig.getInstance();
    /**
     * 使用图片绘制，更为丝滑
     */
    @Getter
    @Setter
    private static BufferedImage image = null;

    @Getter
    @Setter
    private static BufferedImage importImage = null;

    @Override
    public void paint(Graphics g) {
        // super.paint(g);
        g.drawImage(image, 0, 0, null);
    }


    /**
     * 重置画板
     */
    public Graphics reset(Color color) {
        // 创建新的图片画布
        MyCanvas.setImage(new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB));
        // 获取画笔，画笔用于在画布上进行绘制
        Graphics g = image.getGraphics();
        // 设置画笔的颜色
        assert color != null;
        g.setColor(color);
        // 绘制画布的背景色
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        // 更改canvas 的背景
        this.setBackground(color);
        return g;
    }

    public Graphics updateImage(BufferedImage bufferedImage) {
        if (bufferedImage != null) {
            MyCanvas.setImage(bufferedImage);
        }
        return image.getGraphics();
    }

    public Graphics reset() {
        return reset(paintConfig.getBackgroundColor());
    }

    public void redraw() {
        redraw(paintConfig.activate());
    }

    public void redraw(Graphics graphics) {
        shapes.forEach(shape -> {
            shape.show(graphics, false);
        });
    }


    @Override
    public void magic() {
        this.repaint();
    }


    @Override
    public Memento createMemento() {
        return new Memento(shapes);
    }

    @Override
    public void restoreMemento(Memento memento) {
        shapes.clear();
        shapes.addAll(memento.getShapes());
    }

    // =====================================================================
    private MyCanvas() {
    }


    private static class Holder {
        private static final MyCanvas INSTANCE = new MyCanvas();
    }

    public static MyCanvas getInstance() {
        return MyCanvas.Holder.INSTANCE;
    }
}