package com.yisoo.homework.whiteboard.ui.theme;

import com.yisoo.homework.whiteboard.constant.DrawConst;
import com.yisoo.homework.whiteboard.ui.view.MainFrame;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ThemeHandler {

    private ThemeStrategy themeStrategy;

    public void activateTheme(DrawConst.ThemeEnum theme, MainFrame frame){
        this.themeStrategy.activateTheme(theme,frame);
    }
}
