package com.yisoo.homework.whiteboard.ui.component.trans.output;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 11:31
 * @describe PNGMenuItem
 */
public class PNGMenuItem extends JMenuItem {

    public PNGMenuItem(){
        super("导出为PNG");
    }
}
