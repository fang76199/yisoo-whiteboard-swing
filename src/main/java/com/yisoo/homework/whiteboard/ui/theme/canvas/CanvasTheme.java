package com.yisoo.homework.whiteboard.ui.theme.canvas;

import com.yisoo.homework.whiteboard.ui.config.IPaintConfig;
import com.yisoo.homework.whiteboard.ui.config.PaintConfig;
import com.yisoo.homework.whiteboard.utils.CanvasUtils;
import lombok.Data;

import javax.swing.*;
import java.awt.*;

@Data
public abstract class CanvasTheme {
    private Color color;

    private final IPaintConfig paintConfig = PaintConfig.getInstance();

    /**
     * 使主题生效
     * @param canvas 失效，因为使用外观模式优化掉了，不要删，因为有字节码可能会用到
     */
    public void activateCanvasTheme(JPanel canvas){
        CanvasUtils.changeTheme(this.color).addImage().redraw().show();
    }
}
