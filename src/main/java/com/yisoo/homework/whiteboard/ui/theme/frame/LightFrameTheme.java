package com.yisoo.homework.whiteboard.ui.theme.frame;

import com.formdev.flatlaf.FlatLightLaf;

public class LightFrameTheme extends FrameTheme{
    public LightFrameTheme() {
        this.setFlatLaf(new FlatLightLaf());
    }
}
