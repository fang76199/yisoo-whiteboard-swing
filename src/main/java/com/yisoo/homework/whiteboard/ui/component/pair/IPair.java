package com.yisoo.homework.whiteboard.ui.component.pair;

/**
 * TODO
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/22 13:47
 * @update none
 */
public interface IPair {
    /**
     * 创建迭代器
     */
    Iterator createIterator();
}
