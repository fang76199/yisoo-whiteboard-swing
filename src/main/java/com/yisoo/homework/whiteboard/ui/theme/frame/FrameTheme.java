package com.yisoo.homework.whiteboard.ui.theme.frame;


import com.formdev.flatlaf.FlatLaf;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;

@Data
@Slf4j
public abstract class FrameTheme {
    private FlatLaf flatLaf;

    /**
     * 使主题生效
     *
     */
    public void activateFrameTheme(Frame frame) {
        try {
            UIManager.setLookAndFeel(this.flatLaf);
            SwingUtilities.updateComponentTreeUI(frame);
        } catch (Exception ex) {
            log.error("初始化主题失败...");
        }
    }
}
