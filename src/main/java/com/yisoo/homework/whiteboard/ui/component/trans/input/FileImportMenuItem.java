package com.yisoo.homework.whiteboard.ui.component.trans.input;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 11:31
 * @describe FileOuputMenuItem
 */
public class FileImportMenuItem extends JMenuItem {
    public FileImportMenuItem(){
        super("从文件导入");
    }
}
