package com.yisoo.homework.whiteboard.ui.component.shape;

import java.awt.*;

/**
 * TODO
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/21 0:01
 * @update none
 */
public class LineDotShape extends AbstractShape {
    public LineDotShape() {
        super();
    }

    public LineDotShape(int x1, int y1, int x2, int y2) {
        super(x1, y1, x2, y2);
    }


    @Override
    public void draw(Graphics graphics) {
        int len = getX2() - getX1();
        graphics.drawLine(this.getX1(), this.getY1(), this.getX2(), this.getY2());
        graphics.drawLine(this.getX2(), this.getY2(), this.getX2() + len, this.getY2());
        graphics.drawLine(this.getX2(), this.getY2(), this.getX2(), this.getY2() + len);
        graphics.drawLine(this.getX2(), this.getY2(), this.getX2(), this.getY2() - len);
    }
}
