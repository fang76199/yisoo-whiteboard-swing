package com.yisoo.homework.whiteboard.ui.theme.factory;

import com.yisoo.homework.whiteboard.ui.theme.canvas.CanvasTheme;
import com.yisoo.homework.whiteboard.ui.theme.canvas.LightCanvasTheme;
import com.yisoo.homework.whiteboard.ui.theme.frame.FrameTheme;
import com.yisoo.homework.whiteboard.ui.theme.frame.LightFrameTheme;

public class LightThemeFactory extends ThemeFactory{
    @Override
    public CanvasTheme createCanvasTheme() {
        return new LightCanvasTheme();
    }

    @Override
    public FrameTheme createFrameTheme() {
        return new LightFrameTheme();
    }
}
