package com.yisoo.homework.whiteboard.ui.theme.frame;

import com.formdev.flatlaf.FlatLaf;


/**
 * 用于自定义主题，不属于工厂设计模式
 */
public class OtherFrameTheme extends FrameTheme{
    public OtherFrameTheme(FlatLaf flatLaf) {
        this.setFlatLaf(flatLaf);
    }
}
