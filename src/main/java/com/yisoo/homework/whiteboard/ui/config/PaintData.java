package com.yisoo.homework.whiteboard.ui.config;

import com.yisoo.homework.whiteboard.constant.DrawConst;
import lombok.Data;

import java.awt.*;
import java.io.Serializable;

/**
 * 关于设计模式：桥接模式
 *
 * PaintData是AbstractShape的父类，赋予了改变颜色不增加类个数的能力
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/20 23:56
 * @update none
 */
@Data
public abstract class PaintData implements Serializable {

    private final static long serialVersionUID = 1L;
    /**
     * 画笔颜色
     */
    protected Color color;
    /**
     * 画笔尺寸
     */
    protected DrawConst.BrushSizeEnum size;
    /**
     * 画笔风格
     */
    protected DrawConst.BrushStyleEnum style;
    /**
     * 文本框的数据
     */
    protected String wordText;
    /**
     * 主题背景色，用于橡皮擦
     */
    protected Color backgroundColor;
    /**
     * 字体
     */
    protected Font font;
}
