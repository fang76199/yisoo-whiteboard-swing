package com.yisoo.homework.whiteboard.ui.theme.factory;

import com.yisoo.homework.whiteboard.ui.theme.canvas.CanvasTheme;
import com.yisoo.homework.whiteboard.ui.theme.canvas.DarkCanvasTheme;
import com.yisoo.homework.whiteboard.ui.theme.frame.DarkFrameTheme;
import com.yisoo.homework.whiteboard.ui.theme.frame.FrameTheme;

public class DarkThemeFactory extends ThemeFactory{
    @Override
    public CanvasTheme createCanvasTheme() {
        return new DarkCanvasTheme();
    }

    @Override
    public FrameTheme createFrameTheme() {
        return new DarkFrameTheme();
    }
}
