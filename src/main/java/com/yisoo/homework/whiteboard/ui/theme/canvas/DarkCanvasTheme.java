package com.yisoo.homework.whiteboard.ui.theme.canvas;

import java.awt.*;

public class DarkCanvasTheme extends CanvasTheme {
    public DarkCanvasTheme() {
        this.setColor(new Color(40,44,52));
    }
}
