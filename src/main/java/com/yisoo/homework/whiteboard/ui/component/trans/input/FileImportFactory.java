package com.yisoo.homework.whiteboard.ui.component.trans.input;

import com.yisoo.homework.whiteboard.decorator.FileDecorator;
import com.yisoo.homework.whiteboard.decorator.FileImport;
import com.yisoo.homework.whiteboard.ui.component.trans.TransFactory;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 11:41
 * @describe FileImportFactory
 */
public class FileImportFactory implements TransFactory {
    @Override
    public JMenuItem createItem() {
        FileImportMenuItem item = new FileImportMenuItem();

        item.addActionListener(e-> new FileDecorator(new FileImport()).input());

        return item;
    }
}
