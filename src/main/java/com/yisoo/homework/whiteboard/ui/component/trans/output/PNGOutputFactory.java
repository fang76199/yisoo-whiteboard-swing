package com.yisoo.homework.whiteboard.ui.component.trans.output;

import com.yisoo.homework.whiteboard.decorator.PngDecorator;
import com.yisoo.homework.whiteboard.decorator.PngOutput;
import com.yisoo.homework.whiteboard.ui.component.trans.TransFactory;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 11:35
 * @describe PNGOutputFactory
 */
public class PNGOutputFactory implements TransFactory {
    @Override
    public JMenuItem createItem() {
        PNGMenuItem item = new PNGMenuItem();

        item.addActionListener(e-> new PngDecorator(new PngOutput()).output());

        return item;
    }
}
