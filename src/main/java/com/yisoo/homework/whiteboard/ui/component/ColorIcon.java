package com.yisoo.homework.whiteboard.ui.component;

import com.formdev.flatlaf.icons.FlatAbstractIcon;
import lombok.ToString;

import java.awt.*;


/**
 * ColorIcon
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/19 13:28
 * @update none
 */
@ToString
public class ColorIcon extends FlatAbstractIcon {
    private final Color color;
    private final Integer width;
    private final Integer height;

    public ColorIcon(Color color,Integer width,Integer height) {
        super(width, height, null);
        this.color = color;
        this.width = width;
        this.height = height;
    }

    @Override
    protected void paintIcon(Component c, Graphics2D g) {
        g.setColor(this.color);
        g.fillRoundRect(1, 1, this.width-2, this.height-2, 5, 5);
    }
}