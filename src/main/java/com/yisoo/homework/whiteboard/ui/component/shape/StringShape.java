package com.yisoo.homework.whiteboard.ui.component.shape;

import java.awt.*;

/**
 * TODO
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/21 0:01
 * @update none
 */
public class StringShape extends AbstractShape {

    public StringShape() {
        super();
    }

    public StringShape(int x1, int y1, int x2, int y2) {
        super(x1, y1, x2, y2);
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.drawString(this.getWordText(), this.getX1(), this.getY1());
    }
}
