package com.yisoo.homework.whiteboard.ui.component.shape;

import com.yisoo.homework.whiteboard.ui.canvas.MyCanvas;
import com.yisoo.homework.whiteboard.ui.config.PaintConfig;
import com.yisoo.homework.whiteboard.ui.config.PaintData;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.awt.*;

/**
 * 关于设计模式：模板方法模式
 * 主要在show方法
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/20 23:45
 * @update none
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractShape extends PaintData {
    private Integer X1, Y1, X2, Y2;

    private final MyCanvas canvas = MyCanvas.getInstance();
    private final PaintConfig paintConfig = PaintConfig.getInstance();

    public AbstractShape() {
        this.font = paintConfig.getFont();
        this.color = paintConfig.getColor();
        this.size = paintConfig.getSize();
        this.style = paintConfig.getStyle();
        this.wordText = paintConfig.getWordText();
        this.backgroundColor = paintConfig.getBackgroundColor();
    }

    public AbstractShape(int x1, int y1, int x2, int y2) {
        this();
        this.X1 = x1;
        this.X2 = x2;
        this.Y1 = y1;
        this.Y2 = y2;
    }

    public void specialSolveGraphics(Graphics graphics) {
    }

    public abstract void draw(Graphics graphics);

    public void show(Graphics graphics) {
        show(graphics, true);
    }

    public void show(Graphics graphics, Boolean isRepaint) {
        // * 刷新画笔配置
        activateGraphics();
        // * 对画笔特殊处理
        specialSolveGraphics(graphics);
        // * 定制化
        draw(graphics);
        if (isRepaint) {
            // * 绘制
            repaint();
        }
    }

    private void activateGraphics() {
        paintConfig.activate(this);
    }

    public void repaint() {
        canvas.repaint();
    }
}
