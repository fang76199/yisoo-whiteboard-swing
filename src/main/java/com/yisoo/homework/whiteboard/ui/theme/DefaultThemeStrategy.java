package com.yisoo.homework.whiteboard.ui.theme;

import com.yisoo.homework.whiteboard.constant.DrawConst;
import com.yisoo.homework.whiteboard.ui.theme.factory.DarkThemeFactory;
import com.yisoo.homework.whiteboard.ui.theme.factory.LightThemeFactory;
import com.yisoo.homework.whiteboard.ui.theme.factory.ThemeFactory;
import com.yisoo.homework.whiteboard.ui.view.MainFrame;

/**
 * 原生主题策略
 */
public class DefaultThemeStrategy implements ThemeStrategy{
    @Override
    public void activateTheme(DrawConst.ThemeEnum theme, MainFrame frame) {
        // 原生主题有明暗两个，所以要区分
        if (theme == DrawConst.ThemeEnum.DEFAULT_LIGHT) {
            ThemeFactory themeFactory = new LightThemeFactory();
            themeFactory.createFrameTheme().activateFrameTheme(frame);
            themeFactory.createCanvasTheme().activateCanvasTheme(frame.getCanvas());
        } else if (theme == DrawConst.ThemeEnum.DEFAULT_DARK) {
            ThemeFactory themeFactory = new DarkThemeFactory();
            themeFactory.createFrameTheme().activateFrameTheme(frame);
            themeFactory.createCanvasTheme().activateCanvasTheme(frame.getCanvas());
        }
    }
}
