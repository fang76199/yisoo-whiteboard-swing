package com.yisoo.homework.whiteboard.ui.config;

import java.awt.*;

/**
 * IPaintConfig
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/18 23:17
 * @update none
 */
public interface IPaintConfig {

    /**
     * 激活配置
     */
    Graphics activate();

    /**
     * 使用paintData激活
     * @param paintData
     * @return
     */
    Graphics activate(PaintData paintData);

    /**
     * 初始化画笔
     * @param g
     */
    void initGraphics(Graphics g);

    /**
     * 设置画笔尺寸
     * @param size
     */
    void setGraphicsSize(float size);
}
