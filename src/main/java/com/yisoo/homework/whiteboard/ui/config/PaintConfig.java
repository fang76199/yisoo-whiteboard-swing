package com.yisoo.homework.whiteboard.ui.config;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.awt.*;

/**
 * PaintConfig
 * 关于设计模式：单例模式
 * 懒汉式 静态内部类方式
 * 详解：
 * 由于 JVM 在加载外部类的过程中, 是不会加载静态内部类的, 只有内部类的属性/方法被调用时才会被加载, 并初始化其静态属性。
 * 静态属性由于被 `static` 修饰，保证只被实例化一次，并且严格保证实例化顺序。
 * 第一次加载Singleton类时不会去初始化INSTANCE，只有第一次调用getInstance，
 * 虚拟机加载SingletonHolder 并初始化INSTANCE，这样不仅能确保线程安全，也能保证 Singleton 类的唯一性。
 * 静态内部类单例模式是一种优秀的单例模式，是开源项目中比较常用的一种单例模式。
 * 在没有加任何锁的情况下，保证了多线程下的安全，并且没有任何性能影响和空间的浪费。
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/18 23:18
 * @update none
 */
@Data
@Accessors(chain = true)
public class PaintConfig extends PaintData implements IPaintConfig {

    /**
     * 画笔
     */
    @Setter
    @Getter
    private static Graphics graphics = null;

    @Override
    public Graphics activate() {
        // 设置颜色
        graphics.setColor(this.color);
        // 同步修改字体大小
        this.font = this.font.deriveFont((float) (this.size.getValue() + 16));
        // 设置字体
        graphics.setFont(this.font);
        // 设置线宽
        setGraphicsSize(this.size.getValue());

        return graphics;
    }

    /**
     * 外部参数激活，不动全局配置参数，用于重绘
     * @param paintData
     * @return
     */
    @Override
    public Graphics activate(PaintData paintData) {
        // 设置颜色
        graphics.setColor(paintData.color);
        // 同步修改字体大小
        this.font = paintData.font.deriveFont((float) (paintData.size.getValue() + 16));
        // 设置字体
        graphics.setFont(paintData.font);
        // 设置线宽
        setGraphicsSize(paintData.size.getValue());
        return graphics;
    }




    @Override
    public void setGraphicsSize(float size) {
        ((Graphics2D) graphics).setStroke(new BasicStroke(size));
    }

    @Override
    public void initGraphics(Graphics g) {
        // 画笔绑定
        graphics = g;
        // 抗锯齿
        // ((Graphics2D) this.graphics).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        ((Graphics2D) graphics).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
    }


    private PaintConfig(){
        // 没有调自己功能，这里直接配好默认字体
        this.font =  new Font("宋体",Font.PLAIN,16);
    }


    private static class Holder{
        private static final PaintConfig INSTANCE = new PaintConfig();
    }

    public static PaintConfig getInstance() {
        return Holder.INSTANCE;
    }

}
