package com.yisoo.homework.whiteboard.ui.component.trans.output;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 11:31
 * @describe FileOuputMenuItem
 */
public class FileOuputMenuItem extends JMenuItem {
    public FileOuputMenuItem(){
        super("导出为文件");
    }
}
