package com.yisoo.homework.whiteboard.ui.component.trans.output;

import com.yisoo.homework.whiteboard.decorator.FileDecorator;
import com.yisoo.homework.whiteboard.decorator.FileOutput;
import com.yisoo.homework.whiteboard.ui.component.trans.TransFactory;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 11:35
 * @describe FileOutputFactory
 */
public class FileOutputFactory implements TransFactory {
    @Override
    public JMenuItem createItem() {
        FileOuputMenuItem item = new FileOuputMenuItem();

        item.addActionListener(e-> new FileDecorator(new FileOutput()).output());

        return item;
    }
}
