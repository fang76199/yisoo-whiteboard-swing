package com.yisoo.homework.whiteboard.ui.component.trans;

import javax.swing.*;

/**
 * 关于设计模式：工厂方法模式
 *
 *
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 11:26
 * @describe TransFactory
 */
public interface TransFactory {

    JMenuItem createItem();

}
