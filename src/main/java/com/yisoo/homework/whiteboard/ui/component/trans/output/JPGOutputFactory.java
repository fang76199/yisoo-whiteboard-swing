package com.yisoo.homework.whiteboard.ui.component.trans.output;

import com.yisoo.homework.whiteboard.decorator.JpgDecorator;
import com.yisoo.homework.whiteboard.decorator.JpgOutput;
import com.yisoo.homework.whiteboard.ui.component.trans.TransFactory;

import javax.swing.*;

/**
 * @author mmciel 761998179@qq.com
 * @version 1.0
 * @date Created in 2022/12/23 11:35
 * @describe JPGOutputFactory
 */
public class JPGOutputFactory implements TransFactory {
    @Override
    public JMenuItem createItem() {
        JPGMenuItem item = new JPGMenuItem();

        item.addActionListener(e-> new JpgDecorator(new JpgOutput()).output());

        return item;
    }
}
