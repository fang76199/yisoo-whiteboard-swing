package com.yisoo.homework.whiteboard.ui.theme;


import com.yisoo.homework.whiteboard.constant.DrawConst;
import com.yisoo.homework.whiteboard.ui.view.MainFrame;

/**
 * 关于设计模式：使用策略模式激活主题
 *
 * PS：策略+工厂，是消除大规模if-else的利器
 */
public interface ThemeStrategy {
    void activateTheme(DrawConst.ThemeEnum theme, MainFrame frame);
}
