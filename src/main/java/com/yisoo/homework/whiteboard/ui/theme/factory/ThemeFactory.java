package com.yisoo.homework.whiteboard.ui.theme.factory;

import com.yisoo.homework.whiteboard.ui.theme.canvas.CanvasTheme;
import com.yisoo.homework.whiteboard.ui.theme.frame.FrameTheme;

/**
 *
 * 关于设计模式：抽象工厂模式
 *
 * 抽象工厂模式可以用于UI换肤，此处使用抽象工厂模式构建。
 *
 * PS: 使用抽象工厂模式只实现了默认的明亮和暗黑两种风格，其他风格将通过动态代理+字节码生成的方式实现，这么做的原因是减少工厂类与更灵活的扩展。
 *
 */
public abstract class ThemeFactory {

    public abstract CanvasTheme createCanvasTheme();
    public abstract FrameTheme createFrameTheme();
}
