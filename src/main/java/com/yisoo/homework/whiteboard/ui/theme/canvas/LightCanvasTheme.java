package com.yisoo.homework.whiteboard.ui.theme.canvas;

import java.awt.*;

public class LightCanvasTheme extends CanvasTheme {
    public LightCanvasTheme() {
        this.setColor(Color.WHITE);
    }
}
