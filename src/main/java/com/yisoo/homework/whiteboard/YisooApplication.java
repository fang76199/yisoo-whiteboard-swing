package com.yisoo.homework.whiteboard;

import com.yisoo.homework.whiteboard.recovery.Caretaker;
import com.yisoo.homework.whiteboard.recovery.CaretakerProxy;
import com.yisoo.homework.whiteboard.recovery.ICaretaker;
import com.yisoo.homework.whiteboard.ui.view.MainFrame;
import com.yisoo.homework.whiteboard.ui.view.MainFrameDirector;
import lombok.extern.slf4j.Slf4j;


/**
 * YisooApplication
 *
 * @author mmciel 761998179@qq.com
 * @version 1.0.0
 * @date 2022/12/18 20:13
 * @update none
 */
@Slf4j
public class YisooApplication {

    public void init(){
        log.info("===================YisooApplication Starting：Init===================");
        MainFrame frame = (MainFrame)MainFrameDirector.create(new MainFrame());

        log.info("===================YisooApplication Starting：Success===================");
        ICaretaker proxy = (ICaretaker)new CaretakerProxy(new Caretaker()).getProxy();
        log.info("尝试恢复上次编辑");
        proxy.recovery();
    }

    public static void main(String[] args) {
        new YisooApplication().init();
    }
}
